import sys
import string
import math
def jacard(cadena1,cadena2):
    lista1=cadena1.split(" ") 
    lista2=cadena2.split(" ")
    interseccion = set(lista1)& set(lista2)
    #print "I ",interseccion
    union = set(lista1)|set(lista2)
    return float(float(len(interseccion)))/float(float(len(union)))
    
def signopuntuacion(cad):
    cont=0
    for c in string.punctuation:            
        for a in range(len(cad)):
            if c == cad[a]:
                cont+=1
    return cont

def palabrascompartidas(cad1,cad2):
    lista1=cad1.split(" ") 
    lista2=cad2.split(" ")
    interseccion = set(lista1)& set(lista2)
    return int(len(interseccion))
    
def distEuclidiana(P,Q):
    suma=0
    for i in range(len(P)-1):
        suma+=(P[i]-Q[i])**2
    return math.sqrt(suma)
    
def main():
    entrada = open(sys.argv[1],"r")
    #entrada = open("train.input.txt","r")
    salida = open(sys.argv[2],"w")
    #salida = open("salida.txt","w")
    
    #sX[0]=Numero de palabras MAYUSCULAS
    #sX[1]=Numero de palabras minusculas
    #sX[2]=Numero de signos de puntuacion
    #sX[3]=Numero de palabras que comparte
    
    for linea in entrada.readlines():
       linea=linea.replace("\n","")
       print "L | ",linea
       sentencia=linea.split("\t")
       #print "S | ",sentencia
       #print "S[0]=",sentencia[0]
       #print "S[1]=",sentencia[1]
       se0=sentencia[0].split(" ")
       #print se0
       se1=sentencia[1].split(" ")
       #print se1
       s0=[0,0,0,0]
       for i in range(len(se0)):
           if se0[i].isupper(): #mayusculas
               s0[0]+=1
           elif se0[i].islower(): #minusculas
               s0[1]+=1
           s0[2]+=signopuntuacion(se0[i]) #signos de puntuacion
       #print s0
       s1=[0,0,0,0]
       for i in range(len(se1)):
           if se1[i].isupper(): #mayusculas
               s1[0]+=1
           elif se1[i].islower(): #minusculas
               s1[1]+=1
           s1[2]+=signopuntuacion(se1[i]) #signos de puntuacion
       #print s1
       s0[3]=s1[3]=palabrascompartidas(sentencia[0],sentencia[1])
       print "s0=",s0,"\ts1=",s1
       res=distEuclidiana(s0,s1)
       print "distancia euclidiana: ",res

       rjacard = jacard(sentencia[0],sentencia[1])
     #salida.write(linea + "\t" + rjacard + "|" + otra)

   
if __name__ == "__main__":
    main()
 
