#!/usr/bin/python
# -*- coding: utf8 -*-
import sys
import numpy
from decimal import *
import math

getcontext().prec=12

if len(sys.argv) != 2:
  sys.exit('Uso: freq.py texto.txt')

def strToArray(str):
  r = {}
  r.clear()
  i = 0
  l = str.split(' ')
  for k in l:
    r[i] = k
    i = i + 1
  return r

arch = open(sys.argv[1])
h = {}
hb= {}
ht = {}
hc = {}
hq = {}
for linea in arch.xreadlines():
  s = strToArray( linea )
  for i in range(0, len(s)):
    if h.has_key(s[i]):
      h[ s[i] ] = h[ s[i] ] + 1
    else:
      h[ s[i] ] = 1;

  for i in range(0, len(s)-1):
      if hb.has_key(s[i]+" "+s[i+1]):
         hb[s[i]+" "+s[i+1]] = hb[s[i]+" "+s[i+1]]+1
      else:
         hb[s[i]+" "+s[i+1]] = 1



#Cálculo de los trigramas en la tabla hash HT
  for i in range(0, len(s)-2):
    if ht.has_key(s[i]+" "+s[i+1]+" "+s[i+2]):
      ht[s[i]+" "+s[i+1]+" "+s[i+2]] = ht[s[i]+" "+s[i+1]+" "+s[i+2]]+1
    else:
      ht[s[i]+" "+s[i+1]+" "+s[i+2]] = 1


  for i in range(0, len(s)-3):
    if hc.has_key(s[i]+" "+s[i+1]+" "+s[i+2]+" "+s[i+3]):
      hc[s[i]+" "+s[i+1]+" "+s[i+2]+" "+s[i+3]] = hc[s[i]+" "+s[i+1]+" "+s[i+2]+" "+s[i+3]]+1
    else:
       hc[s[i]+" "+s[i+1]+" "+s[i+2]+" "+s[i+3]] = 1


  for i in range(0, len(s)-4):
    if hq.has_key(s[i]+" "+s[i+1]+" "+s[i+2]+" "+s[i+3]+" "+s[i+4]):
      hq[s[i]+" "+s[i+1]+" "+s[i+2]+" "+s[i+3]+" "+s[i+4]] = hq[s[i]+" "+s[i+1]+" "+s[i+2]+" "+s[i+3]+" "+s[i+4]]+1
    else:
       hq[s[i]+" "+s[i+1]+" "+s[i+2]+" "+s[i+3]+" "+s[i+4]] = 1  



#for i in h:
  #print i, h[i]
print "total de unigramas"+ str(len(h))
#for i in hb:
  #print i, hb[i]
print "total de bigramas "+str(len(hb))

print "total de trigramas "+str(len(ht))

print "total de cuatrigramas "+str(len(hc))

print "total de quintigramas "+str(len(hq))

mi = {}

for i in hb:
   palabras=i.split(" ")
   denominador= Decimal(h[palabras[0]])/Decimal(len(h))*Decimal(h[palabras[1]])/Decimal(len(h))
   print denominador
   numerador = Decimal(hb[i])/Decimal(len(hb))
   mi[i]=math.log(Decimal(numerador)/Decimal(denominador))

for i in mi:
  print i + "  " +str(mi[i])
