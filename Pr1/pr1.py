import sys
from math import *

def strToArray(str):
  r = {}
  r.clear()
  i = 0
  l = str.split(' ')
  for k in l:
    r[i] = k
    i = i + 1
  return r

def euclidean(v1, v2):
  return sqrt(sum(pow(a-b,2) for a,b in zip(v1, v2)))

def manhattan_distance(x,y):
  return sum(abs(a-b) for a,b in zip(x,y))

def chebyshev(A,B):
  max = 0
  for i in range(0, len(A)):
    if(A[i]- B[i]>max):
      max = A[i]-B[i]
  return max

def own(A, B):
  res = 0
  max = 0
  min = 0
  for i in range(0, len(A)):
    if(A[i]- B[i] > max):
      max = A[i]-B[i]
    if(A[i]- B[i] < min)
      min = A[i]- B[i]

  if min==0:
    res = 0
  else:
    res = max/min  

  return res

     

entrada = open(sys.argv[1],"r")
salida = open(sys.argv[2],"w")
s1 = {}
s2 = {}

for linea in entrada.readlines():
   linea=linea.replace("\n","")
   sentencia=linea.split("\t")
   #sentenceToVector(sentencia[0],sentencia[1])
   s1 = strToArray(sentencia[0])
   s2 = strToArray(sentencia[1])

h={}
t1 = {}
t2 = {}

for i in range(0, len(s1)):
  if h.has_key(s1[i]):
    h[ s1[i] ] = h[ s1[i] ] + 1
  else:
    h[ s1[i] ] = 1

  if t1.has_key(s1[i]):
    t1[ s1[i] ] = t1[ s1[i] ] + 1
  else:
    t1[ s1[i] ] = 1

for i in range(0, len(s2)):
  if h.has_key(s2[i]):
    h[ s2[i] ] = h[ s2[i] ] + 1
  else:
    h[ s2[i] ] = 1

  if t2.has_key(s2[i]):
    t2[ s2[i] ] = t2[ s2[i] ] + 1
  else:
    t2[ s2[i] ] = 1

h1 = h.copy()
h2 = h.copy()

for i in h1:
  h1[i] = 0

for i in h1:
  for j in t1:
    if(i==j):
      h1[i] = t1[j]

for i in h2:
  h2[i] = 0

for i in h2:
  for j in t2:
    if(i==j):
      h2[i] = t2[j]

vec1 = []
vec2 = []

vc1 =[]
vc2 = []

for i in h1:
  vec1.append(h1[i])

for i in h2:
  vec2.append(h2[i])

for i in h1:
  if(h1[i] != 0):
    vc1.append(1)
  else:
    vc1.append(0)

for i in h2:
  if(h2[i] != 0):
    vc2.append(1)
  else:
    vc2.append(0)


print(euclidean(vc1, vc2))
print(manhattan_distance(vc1, vc2))
print(chebyshev(vc1, vc2))


salida.write(str(h) + "\n\n" + str(t1)+ "\n" + str(t2)+ "\n\n" + str(vec1)+ "\n" + str(vec2)+ "\n\n" + str(vc1)+ "\n" + str(vc2)+ "\n")
salida.write("Distancia Euclidiana: "+ str(euclidean(vc1, vc2))+ "\n" )
salida.write("Distancia de Manhattan: "+ str(manhattan_distance(vc1, vc2))+ "\n" )
salida.write("Distancia chebyshev: "+ str(chebyshev(vc1, vc2))+ "\n" )
#print(linea + "\t" + str(rjacard) + "|")