from pattern.vector import *
from pattern.metrics import *
import networkx as nx
import nltk

def numAdj(sentenciasTag):
    contador=0
    palabras=sentenciasTag[0]#palabras de S1
    adjS1=[]
    for palabra in palabras:
        #palabra[0]  es la palabra
        #palabra[1]  es la etiqueta
        if(palabra[1]=='JJ' or palabra[1]=='JJR' or palabra[1]=='JJS'):
            adjS1.append(palabra[0])
    palabras2=sentenciasTag[1]#palabras de s2
    for palabra in palabras2:
        if(palabra[0] in adjS1):
            contador+=1
    return contador

def numVerb(sentenciasTag):
    contador=0
    palabras=sentenciasTag[0]#palabras de S1
    verS1=[]
    for palabra in palabras:
        #palabra[0]  es la palabra
        #palabra[1]  es la etiqueta
        if(palabra[1]=='VB' or palabra[1]=='VBZ' or palabra[1]=='VBP' or palabra[1]=='VBD' or palabra[1]=='VBN' or palabra[1]=='VBG'):
            verS1.append(palabra[0])
    palabras2=sentenciasTag[1]#palabras de s2
    for palabra in palabras2:
        if(palabra[0] in verS1):
            contador+=1
    return contador

def numSust(sentenciasTag):
    contador=0
    palabras=sentenciasTag[0]#palabras de S1
    susS1=[]
    for palabra in palabras:
        #palabra[0]  es la palabra
        #palabra[1]  es la etiqueta
        if(palabra[1]=='NN' or palabra[1]=='NNS' or palabra[1]=='NNP' or palabra[1]=='NNPS'):
            susS1.append(palabra[0])
    palabras2=sentenciasTag[1]#palabras de s2
    for palabra in palabras2:
        if(palabra[0] in susS1):
            contador+=1
    return contador

#recibe 2 sentencias (string)
#crea su representacion como grafo de coocurrencia con ancho de ventana=1
#devuelve 1 si los grafos pueden ser isomorfos, 0 si no
def isomorficasV1(sentencia1,sentencia2):
    g1=grafoV1(sentencia1)
    g2=grafoV1(sentencia2)
    if(nx.could_be_isomorphic(g1,g2)):
        return 1
    else:
        return 0

#recibe 2 sentencias (string)
#crea su representacion como grafo de coocurrencia con ancho de ventana=2
#devuelve 1 si los grafos pueden ser isomorfos, 0 si no
def isomorficasV2(sentencia1,sentencia2):
    g1=grafoV2(sentencia1)
    g2=grafoV2(sentencia2)
    if(nx.could_be_isomorphic(g1,g2)):
        return 1
    else:
        return 0

#recibe una string y la transforma a su representacion de grafo de coocurrencia con ancho de ventana=1
def grafoV1(sentencia):
    g=nx.Graph()
    pals=words(sentencia)
    for i in range(0,len(pals)-1):
        g.add_edge(pals[i],pals[i+1])
    removerSelfLoops(g)
    return g

#recibe una string y la transforma a su representacion de grafo de coocurrencia con ancho de ventana=2
def grafoV2(sentencia):
    g=nx.Graph()
    pals=words(sentencia)
    for i in range(0,len(pals)-1):
        g.add_edge(pals[i],pals[i+1])
        if(i+2 <= len(pals)-1):
            g.add_edge(pals[i],pals[i+2])
    removerSelfLoops(g)
    return g

#remueve los autociclos en un grafo
def removerSelfLoops(grafo):
    nodos=grafo.nodes()
    for nodo in nodos:
        if(grafo.has_edge(nodo,nodo)):
            grafo.remove_edge(nodo,nodo)

#####   main   #####

#leemos el archivo y lo guardamos en datos
arch=open('train.txt','r')
datos=[]
for line in arch:
    line=line.split('\n')[0]#esta es la linea sin salto de linea
    line=line.split('\t')
    vector=[]
    vector.append(line[0])#s1
    vector.append(line[1])#s2
    vector.append(line[2])#clase
    datos.append(vector)

#etiquetamos todas las sentencias
etiquetas=[]
for data in datos:
    s1=nltk.word_tokenize(data[0])
    s2=nltk.word_tokenize(data[1])
    tags1=nltk.pos_tag(s1)
    tags2=nltk.pos_tag(s2)
    etiqueta=[]
    etiqueta.append(tags1)
    etiqueta.append(tags2)
    etiquetas.append(etiqueta)

#entrenamos una SVM
svm=SVM()

for i in range(0,len(etiquetas)):
    #creamos el vector para entrenar, este sera nuestro vector de caracteristicas
    vector=[]
    vector.append(numAdj(etiquetas[i]))#caracteristica 1
    vector.append(numVerb(etiquetas[i]))#caracteristica 2
    vector.append(numSust(etiquetas[i]))#caracteristica 3
    vector.append(isomorficasV1(datos[i][0],datos[i][1]))#datos[i][0]  S1, datos[i][1]  S2
    vector.append(isomorficasV2(datos[i][0],datos[i][1]))
    #creamos un documento con el que entrenamos la SVM
    #como primer argumento pasamos el vector de caracteristicas
    #como segundo argumento pasamos el tipo o clase
    #en nuestro caso la clase se encuentra en datos[i][2], pero como cadena,
    #lo pasamos a entero con int()
    doc=Document(vector,type=int(datos[i][2]))
    #entrenamos con el documento
    svm.train(doc)

#mostramos las clases encontradas
print str(len(svm.classes))+' clases encontradas:'
print svm.classes

#leemos el test y lo guardamos en datos
arch=open('test.txt','r')
datos=[]
for line in arch:
    line=line.split('\n')[0]#esta es la linea sin salto de linea
    line=line.split('\t')
    vector=[]
    vector.append(line[0])#s1
    vector.append(line[1])#s2
    vector.append(line[2])#clase
    datos.append(vector)

#etiquetamos todas las sentencias
etiquetas=[]
for data in datos:
    s1=nltk.word_tokenize(data[0])
    s2=nltk.word_tokenize(data[1])
    tags1=nltk.pos_tag(s1)
    tags2=nltk.pos_tag(s2)
    etiqueta=[]
    etiqueta.append(tags1)
    etiqueta.append(tags2)
    etiquetas.append(etiqueta)

for i in range(0,len(etiquetas)):
    #creamos el vector para clasificar, este sera nuestro vector de caracteristicas
    vector=[]
    vector.append(numAdj(etiquetas[i]))#caracteristica 1
    vector.append(numVerb(etiquetas[i]))#caracteristica 2
    vector.append(numSust(etiquetas[i]))#caracteristica 3
    vector.append(isomorficasV1(datos[i][0],datos[i][1]))#datos[i][0]  S1, datos[i][1]  S2
    vector.append(isomorficasV2(datos[i][0],datos[i][1]))
    #clasificamos el vector, para esto creamos un documento
    #pasando como unico argumento el vector
    doc=Document(vector)
    #clasificamos el documento y mostramos la clasificacion
    print svm.classify(doc)